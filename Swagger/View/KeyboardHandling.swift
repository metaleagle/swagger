//
//  KeyboardHandling.swift
//  Swagger
//
//  Created by Андрей Данишевский on 16.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit

extension NSNotification.Name {
    public static let UIHideKeyboard = NSNotification.Name("KHideKeyboardNotification")
}


@objc class KeyboardAppearanceHandler: NSObject{
    @IBOutlet var bottomLayoutConstraint: NSLayoutConstraint?
    @IBOutlet var view: UIView?
    
    private lazy var bottomDistance: CGFloat = {
        if let constant = self.bottomLayoutConstraint?.constant {
            return constant
        }
        else {
            return 0.0
        }
    }()
    
    override init() {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardNotification(_:)), name: .UIKeyboardWillChangeFrame, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func keyboardNotification(_ notification: NSNotification){
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
            let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
            let animationCurve:UIViewAnimationOptions = UIViewAnimationOptions(rawValue: animationCurveRaw)
            if (endFrame?.origin.y)! >= UIScreen.main.bounds.size.height {
                self.bottomLayoutConstraint?.constant = self.bottomDistance
            } else {
                if let keyboardHeight = endFrame?.size.height {
                self.bottomLayoutConstraint?.constant = (keyboardHeight + self.bottomDistance)
                }
                else {
                    self.bottomLayoutConstraint?.constant = self.bottomDistance
                }
            }
            UIView.animate(withDuration: duration,
                           delay: TimeInterval(0),
                           options: animationCurve,
                           animations: { self.view?.layoutIfNeeded() },
                           completion: nil)
        }
    }
}

@objc class KeyboardRemover : NSObject{
    @IBOutlet var supervisedElements: [UIControl]?
    
    override init(){
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboadWithNotification(_:)), name: .UIHideKeyboard, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func hideKeyboadWithNotification(_ notification: Notification){
        guard let elements =  self.supervisedElements else {
            return
        }
        for eachElement in elements {
            if eachElement.isFirstResponder{
                if eachElement.canResignFirstResponder {
                    eachElement.resignFirstResponder()
                }
                break
            }
        }
    }
    
}
