//
//  InterfaceBlocking.swift
//  Swagger
//
//  Created by Andrew Danishevskyi on 20.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit

@objc protocol InterfaceBlockerProtocol{
    func disableUIElements(_ isDisabled: Bool)
}

@objc class InterfaceBlocker: NSObject, InterfaceBlockerProtocol{
    @IBOutlet private var elementsToBlock: [UIControl]?
    
    func disableUIElements(_ isDisabled: Bool) {
        if let elements = elementsToBlock{
            for element in elements{
                DispatchQueue.main.async {
                    element.isEnabled = !isDisabled
                }
            }
        }
        
    }
}
