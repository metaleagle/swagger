//
//  TextFieldValidator.swift
//  Swagger
//
//  Created by Андрей Данишевский on 16.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit

@objc protocol ValidatableUIElement {
    func isValid() -> Bool
}

extension UIControl: ValidatableUIElement{
    func isValid() -> Bool {
        return true
    }
}

protocol NamedTextField {
    var fieldName: String { get set }
}

@IBDesignable class ValidatedTextField: UITextField, NamedTextField {
    @IBOutlet var validator: TextFieldValidator?
    
    @IBInspectable var fieldName: String = ""
    
    override func isValid() -> Bool {
        guard let fieldValidator = self.validator else {
            return true
        }
        return fieldValidator.validateTextField(self)
    }
}

@objc protocol TextFieldValidator {
    func validateTextField(_ textField: UITextField) -> Bool
}

class PhoneFieldValidator: NSObject, TextFieldValidator {
    func validateTextField(_ textField: UITextField) -> Bool {
        if let charactersCount = textField.text?.characters.count {
            return charactersCount > 0
        }
        else {
            return false
        }
    }
}


