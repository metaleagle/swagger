//
//  ViewController.swift
//  Swagger
//
//  Created by Андрей Данишевский on 16.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    @IBOutlet var requestModel: RequestModelProtocol!
    @IBOutlet var errorReporter: ErrorReporterProtocol!
    @IBOutlet var interfaceBlocker: InterfaceBlockerProtocol!
    var requestSender: RequestSenderProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        requestModel.addRequestElement(LocationRequestElement(withLocationManager: appDelegate.locationManager))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }

    @IBAction func buttonPush () {
        NotificationCenter.default.post(name: .UIHideKeyboard, object: nil)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        if let reachability = appDelegate.reachability{
            if !reachability.isReachable{
                self.errorReporter.reportError(withTitle: "Network is unavailable!", errorText: "Check your internet connection and try again", inViewController: self)
                return
            }
            if (!requestModel.isFulfilled()){
                if let errorMessage = requestModel.errorMessage{
                    self.errorReporter.reportError(errorMessage, inViewController: self)
                }
                return
            }
            self.requestSender = RequestSender(baseURL: "https://whereu.enkonix.com/", resourcePath: "api/range/", method: .POST, requestBodyString: requestModel.requestBody())
            self.interfaceBlocker.disableUIElements(true)
            self.requestSender?.sendRequest(withSuccess: { (response) in
                
//                print(response.JSONBody())
                self.interfaceBlocker.disableUIElements(false)
                
                switch response.responseStatus{
                case .ERROR:
                    guard let JSON = response.JSONBody(),
                        let errors = JSON["errors"] as? [String: [String]] else {
                            self.errorReporter.reportError("An error occured. Please try again later", inViewController: self)
                            return
                    }
                    for (fieldName, fieldErrors) in errors{
                        if self.errorReporter.reportError(fieldErrors.first!, forFieldWithName: fieldName, inViewController: self){
                            return
                        }
                    }
                case .OK:
                    guard let JSON = response.JSONBody(),
                    let lat = JSON["friend_lat"] as? Double,
                        let long = JSON["friend_long"] as? Double else {
                          self.errorReporter.reportError(withTitle: "Sorry", errorText: "Looks like your friend didn't provide his location information", inViewController: self)
                            return
                    }
                    
                    if let mapViewController = self.storyboard?.instantiateViewController(withIdentifier: "mapViewController") as? MapViewController{
                        let friendCoordinate = CLLocationCoordinate2D.init(latitude: lat, longitude: long)
                        mapViewController.friendCoordinate = friendCoordinate
                        if let myLocation = appDelegate.locationManager.location{
                            let distance = CLLocation(latitude: friendCoordinate.latitude, longitude: friendCoordinate.longitude).distance(from: myLocation)
                            mapViewController.distance = distance
                        }
                        if let navigation = self.navigationController{
                            DispatchQueue.main.async {
                                navigation.pushViewController(mapViewController, animated: true)
                            }
                        }
                        
                    }
                }
                
                }, errorHandler: { (errorMessage) in
                    self.interfaceBlocker.disableUIElements(false)
                    self.errorReporter.reportError(withTitle: "Network error", errorText: errorMessage, inViewController: self)
            })
        }
    }

}

