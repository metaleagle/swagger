//
//  MapViewController.swift
//  Swagger
//
//  Created by Andrew Danishevskyi on 20.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps

class MapViewController: UIViewController{
    var friendCoordinate: CLLocationCoordinate2D?
    var distance: Double?
    var camera: GMSCameraPosition?
    @IBOutlet var distanceLabel: UILabel?
    @IBOutlet var mapView: GMSMapView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.camera = GMSCameraPosition.camera(withTarget: friendCoordinate!, zoom: 14)
        self.mapView?.camera = self.camera!
        
        let marker = GMSMarker()
        marker.position = friendCoordinate!
        marker.title = "Your friend is here"
        marker.map = mapView
        
        distanceLabel?.text = "Distance is \(distance?.truncatingRemainder(dividingBy: 1000)) meters"
        
        if distance! > 1_000 {
            distanceLabel?.text = "Distance is \(Int(distance! / 1000)) KM \(Int(distance!.truncatingRemainder(dividingBy: 1000))) meters"
        } else {
            distanceLabel?.text = "Distance is \(distance!) meters"
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
}

