//
//  UIMediator.swift
//  Swagger
//
//  Created by Андрей Данишевский on 16.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit

class  UIMediator: NSObject {
    @IBOutlet var validatedElements: [ValidatableUIElement]?
    @IBOutlet var enableElements: [UIControl]?
    
    private func setElementsEnabled(_ enabled: Bool) -> Void {
        if let elements = enableElements {
            for eachElement in elements {
                eachElement.isEnabled = enabled
            }
        }
    }
    
    func alterElements() -> Void {
        if let elements = validatedElements {
            for eachElement in elements {
                if !eachElement.isValid(){
                    self.setElementsEnabled(false)
                    return
                }
            }
        }
        self.setElementsEnabled(true)
    }
    
    @IBAction func editingChanged(control: UIControl) -> Void{
        self.alterElements()
    }
}

