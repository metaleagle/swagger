//
//  Model.swift
//  Swagger
//
//  Created by Andrew Danishevskyi on 18.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

@objc protocol RequestModelProtocol {
    func requestBody() -> String
    func isFulfilled() -> Bool
    var errorMessage: String? { get }
    func addRequestElement(_ newElement: RequestElementProtocol)
}

@objc class RequestModel: NSObject, RequestModelProtocol {
    @IBOutlet private var requestElements: [RequestElementProtocol]?
    
    func requestBody() -> String {
        var strings = [String]()
        if let elements = requestElements{
            for requestElement in elements{
                if (requestElement.isFulfilled()){
                    strings.append(requestElement.HTMLBody())
                }
            }
        }
        return "{\(strings.joined(separator: ", "))}"
    }
    
    func addRequestElement(_ newElement: RequestElementProtocol){
        requestElements?.append(newElement)
    }
    
    func isFulfilled() -> Bool {
        guard let elements = self.requestElements else {
            return false
        }
        for requestElement in elements{
            if !requestElement.isFulfilled(){
                return false
            }
        }
        return true
    }
    
    var errorMessage: String?{
        if let elements = self.requestElements{
            for requestElement in elements{
                if !requestElement.isFulfilled(){
                    return requestElement.errorMessage
                }
            }
        }
        
        return nil
    }
}

@objc protocol RequestElementProtocol {
    func isFulfilled() -> Bool
    var errorMessage: String? { get }
    func HTMLBody() -> String
}

extension ValidatedTextField: RequestElementProtocol{
    internal var errorMessage: String? {
        return "\(self.placeholder): incorrect value"
    }

    func isFulfilled() -> Bool {
        return self.isValid()
    }
    
    func HTMLBody() -> String {
        return "\"\(self.fieldName)\":\(self.text != nil ? "\"\(self.text!)\"" : "nil")"
    }
}

class LocationRequestElement: RequestElementProtocol {
    internal func HTMLBody() -> String {
        guard let coordinate = self.locationManager.location?.coordinate else {
            return "\"lat\": nil, \"long\": nil"
        }
        return "\"lat\": \(coordinate.latitude), \"long\": \(coordinate.longitude)"
    }

    unowned let locationManager: CLLocationManager
    
    init(withLocationManager manager: CLLocationManager) {
        self.locationManager = manager
        manager.startUpdatingLocation()
    }
    
    func isFulfilled() -> Bool {
        if self.locationManager.location != nil{
            return true
        }
        return false
    }
    
    var errorMessage: String?{
        let authorizationStatus = CLLocationManager.authorizationStatus()
        switch authorizationStatus {
        case .notDetermined:
            fallthrough
        case .restricted:
            fallthrough
        case .denied:
            return "Location service is unavailable. Check Settings and try again"
        default:
            if (nil == self.locationManager.location){
                return "Your location is being determined. Please try again later"
            }
            else{
                return nil
            }
        }
    }
    
    var elementName: String {
        get {
            return "location"
        }
    }
}
