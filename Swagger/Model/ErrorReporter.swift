//
//  ErrorReporter.swift
//  Swagger
//
//  Created by Andrew Danishevskyi on 19.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit

@objc protocol ErrorReporterProtocol {
    func reportError(withTitle title: String, errorText: String, inViewController viewController: UIViewController)
    func reportError(_ errorText: String, inViewController viewController: UIViewController)
    func reportError(_ errorText: String, forFieldWithName fieldName: String, inViewController viewController: UIViewController) -> Bool
}

@objc protocol ErrorReporterObservedElement {
    var elementName: String { get }
    var elementCaption: String { get }
}

@objc class ErrorReporter: NSObject, ErrorReporterProtocol{
    internal func reportError(withTitle title: String, errorText: String, inViewController viewController: UIViewController) {
        let alert = UIAlertController(title: title, message: errorText, preferredStyle: .alert)
        viewController.present(alert, animated: true, completion: nil)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    }

    internal func reportError(_ errorText: String, inViewController viewController: UIViewController) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Error!", message: errorText, preferredStyle: .alert)
            viewController.present(alert, animated: true, completion: nil)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        }
    }

    @IBOutlet private var observedElements: [ErrorReporterObservedElement]?
    
    func reportError(_ errorText: String, forFieldWithName fieldName: String, inViewController viewController: UIViewController) -> Bool {
        if let elements = observedElements{
            for element in elements{
                if fieldName == element.elementName{
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "\(element.elementCaption) Error!", message: errorText, preferredStyle: .alert)
                        viewController.present(alert, animated: true, completion: nil)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    }
                    return true
                }
            }
        }
        return false
    }
}

extension ValidatedTextField: ErrorReporterObservedElement{
    var elementName: String {
        get {
            return self.fieldName
        }
    }
    
    var elementCaption: String {
        get {
            if let caption = self.placeholder{
                return caption
            } else {
                return self.fieldName
            }
        }
    }
}
