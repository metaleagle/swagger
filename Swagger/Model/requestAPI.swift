//
//  requestAPI.swift
//  Swagger
//
//  Created by Andrew Danishevskyi on 19.02.17.
//  Copyright © 2017 MetalEaglE. All rights reserved.
//

import Foundation
import UIKit

enum RequestMethod: String{
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}

enum ResponseStatus{
    case OK, ERROR
}

protocol ResponseProtocol {
    var responseStatus: ResponseStatus { get }
    func JSONBody() -> [String: AnyObject]?
}

protocol RequestSenderProtocol {
    var baseURL: String { get set }
    var resourcePath: String { get set }
    var method: RequestMethod { get set }
    var requestBodyString: String? { get set }
    func sendRequest(withSuccess success: @escaping (ResponseProtocol)-> Swift.Void, errorHandler: @escaping (String) -> Void) -> Void
}

struct URLResponse: ResponseProtocol {
    internal func JSONBody() -> [String : AnyObject]? {
        var json: [String: AnyObject]!
        do {
            json = try JSONSerialization.jsonObject(with: self.dataBytes!, options: []) as! [String: AnyObject]
        } catch {
            return nil
        }
        return json
    }

    internal var responseStatus: ResponseStatus

    var dataBytes: Data?
}

class RequestSender: NSObject, URLSessionDelegate, RequestSenderProtocol{

    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var baseURL: String
    var resourcePath: String
    var method: RequestMethod
    var requestBodyString: String?
    
    init(baseURL: String, resourcePath: String, method: RequestMethod, requestBodyString: String?) {
        self.baseURL = baseURL
        self.resourcePath = resourcePath
        self.method = method
        self.requestBodyString = requestBodyString
        super.init()
    }
    
    deinit {
        if dataTask != nil {
            dataTask?.cancel()
        }
    }
    
    func sendRequest(withSuccess success: @escaping (ResponseProtocol) -> Void, errorHandler: @escaping (String) -> Void) {
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        guard let requestURL = URL(string: "\(baseURL)\(resourcePath)") else {
            return
        }
        var request = URLRequest(url: requestURL)
        request.httpMethod = self.method.rawValue
        request.httpBody = (nil != self.requestBodyString) ? self.requestBodyString!.data(using: .utf8) : nil
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        dataTask = defaultSession.dataTask(with: request, completionHandler: { (data, response, error) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            if let httpResponse = response as? HTTPURLResponse{
                if let error = error {
                    errorHandler(error.localizedDescription)
                } else {
                    var responseStatus: ResponseStatus
                    switch httpResponse.statusCode{
                    case (200..<400):
                        responseStatus = .OK
                    default:
                        responseStatus = .ERROR
                    }
                    success(URLResponse(responseStatus:responseStatus, dataBytes:data))
                }
                
            }
        })
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        dataTask?.resume()
    }
}
